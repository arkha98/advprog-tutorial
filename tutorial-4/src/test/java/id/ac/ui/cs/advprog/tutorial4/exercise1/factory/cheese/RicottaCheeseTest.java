package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RicottaCheeseTest {
    RicottaCheese ricottaCheese;

    @Before
    public void setUp() {
        ricottaCheese = new RicottaCheese();
    }

    @Test
    public void testToString() {
        assertEquals("Shredded Ricotta", ricottaCheese.toString());
    }
}