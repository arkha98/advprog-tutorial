package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.GruyèreCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.RendangSauce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheesePizzaTest {
    CheesePizza cheesePizza;

    @Before
    public void setUp() {
        cheesePizza = new CheesePizza(new DepokPizzaIngredientStore());
        cheesePizza.setName("Cheese Pizza");
    }

    @Test
    public void testPrepare() {
        cheesePizza.prepare();
        assertEquals(cheesePizza.dough.toString(), new ThickCrustDough().toString());
        assertEquals(cheesePizza.sauce.toString(), new RendangSauce().toString());
        assertEquals(cheesePizza.cheese.toString(), new GruyèreCheese().toString());
    }
}