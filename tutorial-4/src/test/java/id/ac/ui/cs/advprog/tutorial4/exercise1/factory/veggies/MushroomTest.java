package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MushroomTest {
    Mushroom mushroom;

    @Before
    public void setUp() {
        mushroom = new Mushroom();
    }

    @Test
    public void testToString() {
        assertEquals("Mushrooms", mushroom.toString());
    }
}