package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EggplantTest {
    Eggplant eggplant;

    @Before
    public void setUp() {
        eggplant = new Eggplant();
    }

    @Test
    public void testToString() {
        assertEquals("Eggplant", eggplant.toString());
    }
}