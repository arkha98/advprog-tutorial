package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SpinachTest {
    Spinach spinach;

    @Before
    public void setUp() {
        spinach = new Spinach();
    }

    @Test
    public void testToString() {
        assertEquals("Spinach", spinach.toString());
    }
}