package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.GruyèreCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.RendangSauce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VeggiePizzaTest {
    VeggiePizza veggiePizza;

    @Before
    public void setUp() {
        veggiePizza = new VeggiePizza(new DepokPizzaIngredientStore());
        veggiePizza.setName("Veggie Pizza");
    }

    @Test
    public void testPrepare() {
        veggiePizza.prepare();
        assertEquals(veggiePizza.dough.toString(), new ThickCrustDough().toString());
        assertEquals(veggiePizza.sauce.toString(), new RendangSauce().toString());
        assertEquals(veggiePizza.cheese.toString(), new GruyèreCheese().toString());
        assertEquals(veggiePizza.veggies.length, new DepokPizzaIngredientStore().createVeggies().length);
    }
}