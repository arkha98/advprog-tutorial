package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class BloodClamsTest {
    BloodClams bloodClams;

    @Before
    public void setUp() {
        bloodClams = new BloodClams();
    }

    @Test
    public void testToString() {
        assertEquals("Blood Clams from Jakarta Bay", bloodClams.toString());
    }
}