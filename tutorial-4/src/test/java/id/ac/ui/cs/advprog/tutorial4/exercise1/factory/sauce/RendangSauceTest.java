package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RendangSauceTest {
    RendangSauce rendangSauce;

    @Before
    public void setUp() {
        rendangSauce = new RendangSauce();
    }

    @Test
    public void testToString() {
        assertEquals("Rendang Sauce", rendangSauce.toString());
    }
}