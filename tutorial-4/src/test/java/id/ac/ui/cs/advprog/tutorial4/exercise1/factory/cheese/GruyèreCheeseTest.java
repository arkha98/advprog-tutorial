package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GruyèreCheeseTest {
    GruyèreCheese gruyèreCheese;

    @Before
    public void setUp() {
        gruyèreCheese = new GruyèreCheese();
    }

    @Test
    public void testToString() {
        assertEquals("Shredded Gruyère", gruyèreCheese.toString());
    }
}