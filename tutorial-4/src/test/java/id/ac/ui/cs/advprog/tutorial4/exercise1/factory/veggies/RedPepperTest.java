package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RedPepperTest {
    RedPepper redPepper;

    @Before
    public void setUp() {
        redPepper = new RedPepper();
    }

    @Test
    public void testToString() {
        assertEquals("Red Pepper", redPepper.toString());
    }
}