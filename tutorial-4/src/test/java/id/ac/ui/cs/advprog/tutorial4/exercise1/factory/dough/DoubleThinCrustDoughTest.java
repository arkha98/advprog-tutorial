package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DoubleThinCrustDoughTest {
    DoubleThinCrustDough doubleThinCrustDough;

    @Before
    public void setUp() {
        doubleThinCrustDough = new DoubleThinCrustDough();
    }

    @Test
    public void testToString() {
        assertEquals("Double Thin Crust Dough", doubleThinCrustDough.toString());
    }
}