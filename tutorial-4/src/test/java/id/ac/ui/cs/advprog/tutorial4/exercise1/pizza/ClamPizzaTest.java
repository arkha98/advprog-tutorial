package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.GruyèreCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.BloodClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.RendangSauce;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ClamPizzaTest {
    ClamPizza clamPizza;

    @Before
    public void setUp() {
        clamPizza = new ClamPizza(new DepokPizzaIngredientStore());
        clamPizza.setName("Clam Pizza");
    }

    @Test
    public void testPrepare() {
        clamPizza.prepare();
        assertEquals(clamPizza.dough.toString(), new ThickCrustDough().toString());
        assertEquals(clamPizza.sauce.toString(), new RendangSauce().toString());
        assertEquals(clamPizza.cheese.toString(), new GruyèreCheese().toString());
        assertEquals(clamPizza.clam.toString(), new BloodClams().toString());
    }
}