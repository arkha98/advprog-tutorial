package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class OnionTest {
    Onion onion;

    @Before
    public void setUp() {
        onion = new Onion();
    }

    @Test
    public void testToString() {
        assertEquals("Onion", onion.toString());
    }
}