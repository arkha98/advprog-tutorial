package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class DoubleThinCrustDough implements Dough {
    @Override
    public String toString() {
        return "Double Thin Crust Dough";
    }
}
