package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class BloodClams implements Clams {
    @Override
    public String toString() {
        return "Blood Clams from Jakarta Bay";
    }
}
