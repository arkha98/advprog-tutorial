package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class GruyèreCheese implements Cheese {
    @Override
    public String toString() {
        return "Shredded Gruyère";
    }
}
