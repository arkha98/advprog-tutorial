package applicant;

import org.junit.Test;

import static org.junit.Assert.*;

public class ApplicantTest {

    @Test
    public void isCredible() {
        assertTrue(Applicant.isCredible());
    }

    @Test
    public void getCreditScore() {
        assertEquals(700, Applicant.getCreditScore());
    }

    @Test
    public void getEmploymentYears() {
        assertEquals(10, Applicant.getEmploymentYears());
    }

    @Test
    public void hasCriminalRecord() {
        assertTrue(Applicant.hasCriminalRecord());
    }
}