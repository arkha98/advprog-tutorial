import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class ScoreGroupingTest {

    @Test
    public void groupByScores() {
        Map<String, Integer> scores = new HashMap<>();

        scores.put("Alice", 12);
        scores.put("Bob", 15);
        scores.put("Charlie", 11);
        scores.put("Delta", 15);
        scores.put("Emi", 15);
        scores.put("Foxtrot", 11);

        Map<Integer, List<String>> check = new HashMap<>();
        // {11=["Charlie", "Foxtrot"], 12=["Alice"], 15=["Emi", "Bob", "Delta"]}
        List<String> tmp = new ArrayList<>();
        tmp.add("Charlie");
        tmp.add("Foxtrot");
        check.put(11, tmp);
        tmp = new ArrayList<>();
        tmp.add("Alice");
        check.put(12, tmp);
        tmp = new ArrayList<>();
        tmp.add("Emi");
        tmp.add("Bob");
        tmp.add("Delta");
        check.put(15, tmp);

        assertEquals(check, ScoreGrouping.groupByScores(scores));
    }
}